# ToDo

### Build Instructions

Simple ... `javac -cp .:./jars/joda-time-2.5.jar self/vpalepu/cli/*.java self/vpalepu/todo/*.java`

### Run Instructions

- using Main Menu ... `java  -cp .:./jars/joda-time-2.5.jar self.vpalepu.todo.App`.
- using Command Line Interface; e.g.:
    - Help! : `java  -cp .:./jars/joda-time-2.5.jar self.vpalepu.todo.App -h`
    - Add new tasks; then mark some tasks as done: `java self.vpalepu.todo.App -n -d`
    - Add new tasks; then mark some tasks as done; then marks some tasks as undone. : `java -cp .:./jars/joda-time-2.5.jar self.vpalepu.todo.App -n -d -u`
    - Add new tasks, then mark some tasks as done; then marks some tasks as undone; then mark some tasks as done, again : `java -cp .:./jars/joda-time-2.5.jar self.vpalepu.todo.App -n -d -u -d`
    - Show all tasks: `java -cp .:./jars/joda-time-2.5.jar self.vpalepu.todo.App -s`

CLI Arguments ( or what I call, Help Sheet):
``` 
:::tex
-h or --help : Print this menu. 
-n or --new : Create new tasks. 
-d or --done : Mark tasks as Done. 
-u or --undone : Mark tasks as Undone. 
-s or --show : Show all tasks. 
-i or --import : Import tasks from a `.todo` file. 
-e or --export : Export tasks to a `.todo` file.
```


### Notes
- Persists Task Lists across ToDo sessions/instances by creating a file called default.todo in the current directory from where the application is invoked.
- *DEPENDENCY* JodaTime v2.5; Jar file donwloadable from: <https://github.com/JodaOrg/joda-time/releases>

### Notice

This project was created out of sheer boredom and frustration.