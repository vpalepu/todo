package self.vpalepu.todo;

public enum TaskStatus {
	Done, Todo;

  public static TaskStatus fromString(String text) {
    String status = text.trim().toUpperCase();
    switch(status) {
      case "[X": case "DONE": return Done;
      case "[": case "TODO": return Todo;
      default:
         throw new RuntimeException("Unknown Status: " + status); 
    }
  }

  public String getSym() {
    switch(this) {
      case Done: return "[x";
      case Todo: return "[ ";
      default:
         throw new RuntimeException("Unknown Status: " + this); 
    }
  }
}