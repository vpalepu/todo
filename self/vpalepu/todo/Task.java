package self.vpalepu.todo;

import org.joda.time.DateTime;
import org.joda.time.DateTime.Property;

public class Task {
	private final String title;
	private TaskStatus status;
	private DateTime datetime;

	public static Task createNew(String taskTitle) {
		Task task = new Task(taskTitle, TaskStatus.Todo);
		return task;
	}

	public static Task fromString(String text) {
		Task task = null;
		String[] split = text.split("\\] ");
		TaskStatus status = TaskStatus.fromString(split[0]);
		String taskTitle = split[1];
		
		if(split.length < 3) {
			task = new Task(taskTitle, status);
		}

		if(split.length == 3) {
			DateTime datetime = DateTime.parse(split[2]);
			task = new Task(taskTitle, status, datetime);
		}

		return task;
	}

	private Task(String title) {
		this.title = title;
		this.status = TaskStatus.Todo;
		this.datetime = DateTime.now();
	}

	private Task(String title, TaskStatus status) {
		this.title = title;
		this.status = status;
		this.datetime = DateTime.now();
	}

	private Task(String title, TaskStatus status, DateTime datetime) {
		this.title = title;
		this.status = status;
		this.datetime = datetime;
	}

	public String title() {
		return title;
	}

	public TaskStatus status() {
		return status;
	}

	public void isDone() {
		this.status = TaskStatus.Done;
		this.datetime = DateTime.now();
	}

	public void isNotDone() {
		this.status = TaskStatus.Todo;
		this.datetime = DateTime.now();
	}

	public String toPrettyString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(status.getSym())
			    .append("] ")
			    .append(title)
			    .append("\n\tLast Updated:")
			    .append(prettyPrint(datetime));

		return buffer.toString();
	}

		private String prettyPrint(DateTime datetime) {
			String mins = datetime.minuteOfHour().getAsShortText();
			String hour = datetime.hourOfDay().getAsShortText();
			String day = datetime.dayOfMonth().getAsShortText();
			String month = datetime.monthOfYear().getAsShortText();
			String year = datetime.year().getAsShortText();

			long instant = datetime.getMillis();
			String zone = datetime.getZone().getShortName(instant);
			
			StringBuffer buffer = new StringBuffer();
			buffer.append(day).append("-")
						.append(month).append("-")
						.append(year).append(" ")
						.append(hour).append(":")
						.append(mins).append(" ")
						.append(zone);
			return buffer.toString();
		}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(status.getSym())
			    .append("] ")
			    .append(title)
			    .append("] ")
			    .append(datetime);

		return buffer.toString();
	}
}