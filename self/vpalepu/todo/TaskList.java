package self.vpalepu.todo;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.PrintStream;
import java.io.IOException;

public class TaskList {

	public static final String DEFAULT_FILE_NAME = "default.todo";

	ArrayList<Task> tasks = new ArrayList<>();

	public static final TaskList loadFromDefault() throws IOException {
		File file = defaultFile();
		TaskList tasks = fromFile(file);
		return tasks;
	}

	public static final void storeToDefault(TaskList list) throws IOException {
		PrintStream out = new PrintStream(defaultFile());
		for (Task task : list.tasks) {
			out.println(task.toString());
		}
	} 

		private static final File defaultFile() throws IOException {
			File file = new File(DEFAULT_FILE_NAME);
			if(!file.exists()) {
				file.createNewFile();
			}
			return file;
		}

	public static final TaskList fromFile(File taskListFile) throws IOException {
		Scanner scanner = new Scanner(taskListFile);
		TaskList list = new TaskList();
		while(scanner.hasNextLine()) {
			String line = scanner.nextLine();
			Task task = Task.fromString(line);
			list.tasks.add(task);
		}
		return list;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("\nCurrent Tasks:\n");

		for (Task task : tasks) {
			buffer.append(task.toString())
						.append("\n");
		}

		return buffer.toString();
	}

	public void print() {
		int index = 1;
		System.out.println("Current Tasks:");

		if(tasks == null || tasks.isEmpty()) {
			System.out.println("- EMPTY LIST.\n");
			return;
		}

		for (Task task : tasks) {
			StringBuffer buffer = new StringBuffer();
			buffer.append(index).append(") ").append(task.toPrettyString());
			System.out.println(buffer.toString());
			index += 1;
		}

		System.out.println("\n");
	}

	public void putTask(String taskTitle) {
		Task task = Task.createNew(taskTitle);
		tasks.add(task);	
	}

	public void setTaskStatus(int taskId, TaskStatus status) throws TaskUnavailableException {
		Task task = null;
		try {
			task = tasks.get(taskId);
		} catch (IndexOutOfBoundsException e) { 
			throw new TaskUnavailableException();
		}
		
		if(task == null) {
			throw new TaskUnavailableException();
		}

		switch (status) {
			case Done: task.isDone(); break;
			case Todo: task.isNotDone(); break;
			default: 
				throw new RuntimeException(status.toString());
		}
		
	}

	public int size() {
		return tasks.size();
	}

	public static class TaskUnavailableException extends Exception {}

}