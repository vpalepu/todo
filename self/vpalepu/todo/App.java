package self.vpalepu.todo;

import static self.vpalepu.cli.Command.HELP;
import static self.vpalepu.cli.Command.TASK;
import static self.vpalepu.cli.Command.DONE;
import static self.vpalepu.cli.Command.REDO;
import static self.vpalepu.cli.Command.SHOW;
import static self.vpalepu.cli.Command.GULP;
import static self.vpalepu.cli.Command.SPIT;
import static self.vpalepu.cli.Command.EXIT;

import java.util.Scanner;
import java.util.ArrayList;
import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import self.vpalepu.cli.Command;
import self.vpalepu.cli.Parser;

public class App {

	public static void execute(String choice, TaskList list) throws IOException {
		switch (choice) {
				case HELP:
					System.out.println(Command.spec());
					break;
 				case TASK: 
 					addNewTask(list);
 					break;
 				case DONE:
 					updateTasks(list, TaskStatus.Done); 
 					break;
 				case REDO:
 					updateTasks(list, TaskStatus.Todo); 
 					break;
 				case SHOW:
 					list.print();
 					askUserInput("Hit any key to continue ...");
 					break;
 				case GULP:
 					list.print();
 					break;
 				case SPIT:
 					list.print();
 					break;
 				case EXIT: 
 					TaskList.storeToDefault(list);
 					exit();
 			}
	}

	public static void main(String[] args) throws IOException {

		System.out.println(DateTime.now(DateTimeZone.UTC));

		TaskList list = TaskList.loadFromDefault();

		if(args != null && args.length != 0) {
			Parser commandlineParser = new Parser(null, args);
			ArrayList<Command> commands = commandlineParser.parse();
			for (Command command : commands) {
				System.out.println(command.toString());
				execute(command.operation(), list);
			}

			TaskList.storeToDefault(list);
			return;
		}

		System.out.println("ToDo.\n=====\n");
 		while(true) {
 			printMainMenu(list);
 			String choice = askUserChoice("What's next? (1-7)");
 			execute(choice, list);
 		}
	}

	private static void printMainMenu(TaskList list) {
		System.out.println("MAIN MENU");
		System.out.println("1) Create New Tasks");
		System.out.println("2) Complete Tasks");
		System.out.println("3) Revive Tasks");
		System.out.println("4) Show Task List");
		System.out.println("5) Import Tasks");
		System.out.println("6) Export Tasks");
		System.out.println("7) Exit");
	}

	private static void exit() {
		System.out.print("Goodbye");
		try {
			Thread.sleep(100);
			System.out.print("!!!");
			Thread.sleep(900);
		} catch(InterruptedException ex) { }

		System.out.println();
		System.exit(0);
	}

	private static void addNewTask(TaskList list) {
		String choice = "Y";
		while(choice.startsWith("Y")) {
			list.print();
			String title = askUserInput("Enter the title of your Task:");
			list.putTask(title);
			choice = askUserChoice("Add more tasks? (y/N)");
		}
		list.print();
	}

	private static void updateTasks(TaskList list, TaskStatus status) {
		String choice = "Y";
		while(choice.startsWith("Y")) {
			list.print();
			String taskId = askUserInput("Enter Task#:");
			
			try {
				int id = Integer.parseInt(taskId);
				list.setTaskStatus(id - 1, status);
			} catch(TaskList.TaskUnavailableException unEx) {
				System.out.println("Task is unavailable. \n You need to enter a number between 1 and " + list.size() + ".");
			} catch (NumberFormatException numEx) {
				System.out.println("You need to enter a number.");
			}

			choice = askUserChoice("Complete other tasks? (y/N)");
		}
		list.print();
	}

	private static String askUserChoice(String question) {
		System.out.print(question + " ");
		String line = new Scanner(System.in).nextLine();
		String choice = line.trim().toUpperCase();
		return choice;
	}

	private static String askUserInput(String message) {
		System.out.print(message);
		Scanner scanner = new Scanner(System.in);
		String line = scanner.nextLine();
		String input = line.trim();
		return input;		
	}
}