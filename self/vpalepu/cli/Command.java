package self.vpalepu.cli;

public class Command {

  public static final String HELP = "0";
  public static final String TASK = "1";
  public static final String DONE = "2";
  public static final String REDO = "3";
  public static final String SHOW = "4";
  public static final String GULP = "5";
  public static final String SPIT = "6";
  public static final String EXIT = "7";

  private final String operationId;

  private Command(String operationId) {
    this.operationId = operationId;
  }

  public String operation() {
    return operationId;
  }

  public static Command fromLong(String string) {
    switch(string) {
     case "--help": return new Command(HELP);
     case "--new": return new Command(TASK);
     case "--done": return new Command(DONE);
     case "--undone": return new Command(REDO);
     case "--show": return new Command(SHOW);
     case "--import": return new Command(GULP);
     case "--export": return new Command(SPIT);
     default: return null;
    }
  }

    public static Command fromShort(String string) {
    switch(string) {
     case "-h": return new Command(HELP);
     case "-n": return new Command(TASK);
     case "-d": return new Command(DONE);
     case "-u": return new Command(REDO);
     case "-s": return new Command(SHOW);
     case "-i": return new Command(GULP);
     case "-e": return new Command(SPIT);
     default: return null;
    }
  }

  public static String spec() {
    String helpSheet = 
      "-h or --help : Print this menu. \n" +
      "-n or --new : Create new tasks. \n" +
      "-d or --done : Mark tasks as Done. \n" +
      "-u or --undone : Mark tasks as Undone. \n" +
      "-s or --show : Show all tasks. \n" +
      "-i or --import : Import tasks from a `.todo` file. \n" +
      "-e or --export : Export tasks to a `.todo` file.";
    return helpSheet;
  }

  @Override
  public String toString() {
    switch(operationId) {
     case HELP: return "HELP";
     case TASK: return "TASK";
     case DONE: return "DONE";
     case REDO: return "REDO";
     case SHOW: return "SHOW";
     case GULP: return "GULP";
     case SPIT: return "SPIT";
     case EXIT: return "EXIT";
     default: throw new RuntimeException("Unknown command: " + operationId);
    }
  }

}