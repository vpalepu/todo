package self.vpalepu.cli;

import java.util.ArrayList;

public class Parser {
  public final String commandLine;
  public final String[] commandLineArray;

  public Parser(String line, String[] lineArray) {
    this.commandLine = line;
    this.commandLineArray = lineArray;
  }

  public ArrayList<Command> parse() {
    ArrayList<Command> commands = new ArrayList<>();

    ArrayList<String> splits = split();
    for(String split : splits) {
      Command command = null;
      if(split.startsWith("--")) {
        command = Command.fromLong(split);
      } else if(split.startsWith("-")) {
        command = Command.fromShort(split);
      } else {
        continue;
      }

      if(command == null) continue;

      commands.add(command);
    }

    return commands;
  }



  private ArrayList<String> split() {
    ArrayList<String> result = new ArrayList<>();

    String[] split = (commandLine != null) ? 
                     commandLine.split("\\s+") :
                     commandLineArray;

    if(split == null) return result;

    for(String s : split) {
      if(s == null || s.isEmpty()) {
        continue;
      }

      result.add(s);
    }
    return result;
  }

}